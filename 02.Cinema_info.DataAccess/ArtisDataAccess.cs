﻿using System;
using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class ArtisDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(ArtisViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    tbl_m_artis ada = db.tbl_m_artis.Where(
                        o => o.nama_artis == paramModel.nama_artis && o.jenis_kelamin == paramModel.jenis_kelamin).FirstOrDefault();

                    if (ada == null)
                    {

                    }
                    tbl_m_artis a = new tbl_m_artis();
                    a.kode_artis = "KODE";
                    a.nama_artis = paramModel.nama_artis;
                    a.jenis_kelamin = paramModel.jenis_kelamin;
                    a.bayaran = paramModel.bayaran;
                    a.award = paramModel.award;
                    a.id_negara = paramModel.id_negara;
                    a.is_active = true;
                    a.created_by = paramModel.created_by;
                    a.created_date = DateTime.UtcNow;
                    a.updated_by = paramModel.created_by;
                    a.updated_date = DateTime.UtcNow;
                    db.tbl_m_artis.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<ArtisViewModel> GetList(string paramSearch, int paramPage, int paramPageSize)
        {
            List<ArtisViewModel> result = new List<ArtisViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<ArtisViewModel> query;
                query = (from a in db.tbl_m_artis
                         where a.is_active
                         && a.nama_artis.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_artis ascending
                         select new ArtisViewModel
                         {
                             id_artis_pk = a.id_artis_pk,
                             nama_artis = a.nama_artis,
                             jenis_kelamin= a.jenis_kelamin,
                             bayaran = a.bayaran,
                             award = a.award,
                             id_negara = a.id_negara
                         }).Skip(paramPage).Take(paramPageSize);

                result = query.ToList();
                foreach (var item in result)
                {
                    item.nama_negara = NegaraDataAccess.GetById(item.id_negara).nama_negara;
                }
            }
            return result;
        }

        public static int GetCount(string paramSearch)
        {
            int countData = 0;
            using (var db = new CinemaEntities())
            {
                countData = (from a in db.tbl_m_artis
                             where a.is_active
                             && a.nama_artis.ToLower().Trim().Contains(paramSearch)
                             select new ArtisViewModel { }).Count();
            }
            return countData;
        }

        public static List<ArtisViewModel> GetAll()
        {
            List<ArtisViewModel> result = new List<ArtisViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<ArtisViewModel> query;
                query = (from a in db.tbl_m_artis
                         where a.is_active                         
                         select new ArtisViewModel
                         {
                             id_artis_pk = a.id_artis_pk,
                             nama_artis = a.nama_artis,
                             jenis_kelamin = a.jenis_kelamin,
                             bayaran = a.bayaran,
                             award = a.award,
                             id_negara = a.id_negara
                         });

                result = query.ToList();
                foreach (var item in result)
                {
                    item.nama_negara = NegaraDataAccess.GetById(item.id_negara).nama_negara;
                }
            }
            return result;
        }

        public static ArtisViewModel GetById(int id)
        {
            ArtisViewModel result = new ArtisViewModel();
            using (var db = new CinemaEntities())
            {
                result = (from a in db.tbl_m_artis
                         where a.is_active
                         && a.id_artis_pk == id
                         select new ArtisViewModel
                         {
                             id_artis_pk = a.id_artis_pk,
                             nama_artis = a.nama_artis,
                             jenis_kelamin = a.jenis_kelamin,
                             bayaran = a.bayaran,
                             award = a.award,
                             id_negara = a.id_negara
                         }).FirstOrDefault();

                result.nama_negara = NegaraDataAccess.GetById(result.id_negara).nama_negara;
            }
            return result;
        }

        public static List<ArtisViewModel> GetSearch(string paramSearch)
        {
            List<ArtisViewModel> result = new List<ArtisViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<ArtisViewModel> query;
                query = (from a in db.tbl_m_artis
                         where a.is_active
                         && a.nama_artis.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_artis ascending
                         select new ArtisViewModel
                         {
                             id_artis_pk = a.id_artis_pk,
                             nama_artis = a.nama_artis,
                             jenis_kelamin = a.jenis_kelamin,
                             bayaran = a.bayaran,
                             award = a.award,
                             id_negara = a.id_negara
                         });

                result = query.ToList();
            }
            return result;
        }

        public static bool CheckArtis(string nama, string jk, int id_negara)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                tbl_m_artis a = db.tbl_m_artis.Where(
                    o =>
                    o.nama_artis.ToLower() == nama.ToLower() && o.jenis_kelamin == jk && o.id_negara == id_negara
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
