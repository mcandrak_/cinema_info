﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class DetailGenreFilmDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(DetailGenreFilmViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    detail_genre_film a = new detail_genre_film();
                    a.id_film = paramModel.id_film;
                    a.id_genre = paramModel.id_genre;
                    db.detail_genre_film.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<DetailGenreFilmViewModel> GetListByIdFilm(int idFilm)
        {
            List<DetailGenreFilmViewModel> result = new List<DetailGenreFilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<DetailGenreFilmViewModel> query;
                query = (from a in db.detail_genre_film
                         where a.id_film == idFilm
                         select new DetailGenreFilmViewModel
                         {
                             id = a.id,
                             id_film = a.id_film,
                             id_genre = a.id_genre,
                         });

                result = query.ToList();

                foreach (var item in result)
                {
                    item.nama_genre = GenreDataAccess.GetById((int)item.id_genre).nama_genre;
                }
            }
            return result;
        }

        public static List<DetailGenreFilmViewModel> GetListByIdArtis(int idGenre)
        {
            List<DetailGenreFilmViewModel> result = new List<DetailGenreFilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<DetailGenreFilmViewModel> query;
                query = (from a in db.detail_genre_film
                         where a.id_genre == idGenre
                         select new DetailGenreFilmViewModel
                         {
                             id = a.id,
                             id_genre = a.id_genre,
                             id_film = a.id_film,
                         });

                result = query.ToList();
                foreach (var item in result)
                {
                    item.nama_film = FilmDataAccess.GetById((int)item.id_film).nama_film;
                }
            }
            return result;
        }
    }
}
