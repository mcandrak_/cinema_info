﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class ProduserDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(ProduserViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    tbl_m_produser a = new tbl_m_produser();
                    a.kode_produser = "KODE";
                    a.nama_produser = paramModel.nama_produser;
                    a.international = "Inter";
                    a.id_negara = paramModel.id_negara;
                    a.is_active = true;
                    a.created_by = paramModel.created_by;
                    a.created_date = DateTime.UtcNow;
                    a.updated_by = paramModel.created_by;
                    a.updated_date = DateTime.UtcNow;
                    db.tbl_m_produser.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<ProduserViewModel> GetList(string paramSearch, int paramPage, int paramPageSize)
        {
            List<ProduserViewModel> result = new List<ProduserViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<ProduserViewModel> query;
                query = (from a in db.tbl_m_produser
                         where a.is_active
                         && a.nama_produser.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_produser ascending
                         select new ProduserViewModel
                         {
                             id_produser_pk = a.id_produser_pk,
                             nama_produser = a.nama_produser,
                             id_negara = a.id_negara
                         }).Skip(paramPage).Take(paramPageSize);

                result = query.ToList();

                foreach (var item in result)
                {
                    item.nama_negara = NegaraDataAccess.GetById(item.id_negara).nama_negara;
                }
            }
            return result;
        }

        public static int GetCount(string paramSearch)
        {
            int countData = 0;
            using (var db = new CinemaEntities())
            {
                countData = (from a in db.tbl_m_produser
                             where a.is_active
                             && a.nama_produser.ToLower().Trim().Contains(paramSearch)
                             select new ArtisViewModel { }).Count();
            }
            return countData;
        }

        public static List<ProduserViewModel> GetAll()
        {
            List<ProduserViewModel> result = new List<ProduserViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<ProduserViewModel> query;
                query = (from a in db.tbl_m_produser
                         where a.is_active
                         select new ProduserViewModel
                         {
                             id_produser_pk = a.id_produser_pk,
                             nama_produser = a.nama_produser,
                             id_negara = a.id_negara
                         });
                result = query.ToList();
                foreach (var item in result)
                {
                    item.nama_negara = NegaraDataAccess.GetById(item.id_negara).nama_negara;
                }
            }
            return result;
        }

        public static List<ProduserViewModel> GetSearch(string paramSearch)
        {
            List<ProduserViewModel> result = new List<ProduserViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<ProduserViewModel> query;
                query = (from a in db.tbl_m_produser
                         where a.is_active
                         && a.nama_produser.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_produser ascending
                         select new ProduserViewModel
                         {
                             id_produser_pk = a.id_produser_pk,
                             nama_produser = a.nama_produser,
                             id_negara = a.id_negara
                         });

                result = query.ToList();
            }
            return result;
        }

        public static ProduserViewModel GetById(int id)
        {
            ProduserViewModel result = new ProduserViewModel();
            using (var db = new CinemaEntities())
            {
                result = (from a in db.tbl_m_produser
                         where a.is_active   
                         && a.id_produser_pk == id
                         select new ProduserViewModel
                         {
                             id_produser_pk = a.id_produser_pk,
                             nama_produser = a.nama_produser,
                             id_negara = a.id_negara
                         }).FirstOrDefault();

                result.nama_negara = NegaraDataAccess.GetById(result.id_negara).nama_negara;
            }
            return result;
        }

        public static bool CheckProd(string nama)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                tbl_m_produser a = db.tbl_m_produser.Where(
                    o =>
                    o.nama_produser.ToLower() == nama.ToLower() 
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
