﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class GenreDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(GenreViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    tbl_m_genre a = new tbl_m_genre();
                    a.kode_genre = "KODE";
                    a.nama_genre = paramModel.nama_genre;
                    a.is_active = true;
                    a.created_by = paramModel.created_by;
                    a.created_date = DateTime.UtcNow;
                    a.updated_by = paramModel.created_by;
                    a.updated_date = DateTime.UtcNow;
                    db.tbl_m_genre.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<GenreViewModel> GetList(string paramSearch, int paramPage, int paramPageSize)
        {
            List<GenreViewModel> result = new List<GenreViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<GenreViewModel> query;
                query = (from a in db.tbl_m_genre
                         where a.is_active
                         && a.nama_genre.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_genre ascending
                         select new GenreViewModel
                         {
                             id_genre_pk = a.id_genre_pk,
                             nama_genre = a.nama_genre,
                         }).Skip(paramPage).Take(paramPageSize);

                result = query.ToList();
            }
            return result;
        }

        public static int GetCount(string paramSearch)
        {
            int countData = 0;
            using (var db = new CinemaEntities())
            {
                countData = (from a in db.tbl_m_genre
                             where a.is_active
                             && a.nama_genre.ToLower().Trim().Contains(paramSearch)
                             select new ArtisViewModel { }).Count();
            }
            return countData;
        }

        public static List<GenreViewModel> GetAll()
        {
            List<GenreViewModel> result = new List<GenreViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<GenreViewModel> query;
                query = (from a in db.tbl_m_genre
                         where a.is_active                         
                         orderby a.nama_genre ascending
                         select new GenreViewModel
                         {
                             id_genre_pk = a.id_genre_pk,
                             nama_genre = a.nama_genre,
                         });
                result = query.ToList();
            }
            return result;
        }

        public static List<GenreViewModel> GetSearch(string paramSearch)
        {
            List<GenreViewModel> result = new List<GenreViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<GenreViewModel> query;
                query = (from a in db.tbl_m_genre
                         where a.is_active
                         && a.nama_genre.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_genre ascending
                         select new GenreViewModel
                         {
                             id_genre_pk = a.id_genre_pk,
                             nama_genre = a.nama_genre,
                         });
                result = query.ToList();
            }
            return result;
        }

        public static GenreViewModel GetById(int id)
        {
            GenreViewModel result = new GenreViewModel();
            using (var db = new CinemaEntities())
            {
                result = (from a in db.tbl_m_genre
                         where a.is_active
                         && a.id_genre_pk == id
                         orderby a.nama_genre ascending
                         select new GenreViewModel
                         {
                             id_genre_pk = a.id_genre_pk,
                             nama_genre = a.nama_genre,
                         }).FirstOrDefault();                
            }
            return result;
        }

        public static bool CheckGenre(string nama)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                tbl_m_genre a = db.tbl_m_genre.Where(
                    o =>
                    o.nama_genre.ToLower() == nama.ToLower()
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
