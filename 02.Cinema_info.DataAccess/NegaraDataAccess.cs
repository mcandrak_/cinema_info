﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class NegaraDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(NegaraViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    tbl_m_negara a = new tbl_m_negara();
                    a.kode_negara = "KODE";
                    a.nama_negara = paramModel.nama_negara.ToUpper();
                    a.is_active = true;
                    a.created_by = paramModel.created_by;
                    a.created_date = DateTime.UtcNow;
                    a.updated_by = paramModel.created_by;
                    a.updated_date = DateTime.UtcNow;
                    db.tbl_m_negara.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<NegaraViewModel> GetList(string paramSearch, int paramPage, int paramPageSize)
        {
            List<NegaraViewModel> result = new List<NegaraViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<NegaraViewModel> query;
                query = (from a in db.tbl_m_negara
                         where a.is_active
                         && a.nama_negara.ToLower().Trim().Contains(paramSearch)
                         orderby a.nama_negara ascending
                         select new NegaraViewModel
                         {
                             id_negara_pk = a.id_negara_pk,
                             nama_negara = a.nama_negara,
                         }).Skip(paramPage).Take(paramPageSize);

                result = query.ToList();
            }
            return result;
        }

        public static int GetCount(string paramSearch)
        {
            int countData = 0;
            using (var db = new CinemaEntities())
            {
                countData = (from a in db.tbl_m_negara
                             where a.is_active
                             && a.nama_negara.ToLower().Trim().Contains(paramSearch)
                             select new ArtisViewModel { }).Count();
            }
            return countData;
        }

        public static List<NegaraViewModel> GetAll()
        {
            List<NegaraViewModel> result = new List<NegaraViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<NegaraViewModel> query;
                query = (from a in db.tbl_m_negara
                         where a.is_active                         
                         orderby a.nama_negara ascending
                         select new NegaraViewModel
                         {
                             id_negara_pk = a.id_negara_pk,
                             nama_negara = a.nama_negara,
                         });

                result = query.ToList();
            }
            return result;
        }

        public static NegaraViewModel GetById(int id)
        {
            NegaraViewModel result = new NegaraViewModel();
            using (var db = new CinemaEntities())
            {
                result = (from a in db.tbl_m_negara
                         where a.is_active
                         && a.id_negara_pk == id
                         orderby a.nama_negara ascending
                         select new NegaraViewModel
                         {
                             id_negara_pk = a.id_negara_pk,
                             nama_negara = a.nama_negara,
                         }).FirstOrDefault();
            }
            return result;
        }

        public static bool CheckNegara(string nama)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                tbl_m_negara a = db.tbl_m_negara.Where(
                    o =>
                    o.nama_negara.ToLower() == nama.ToLower() 
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
