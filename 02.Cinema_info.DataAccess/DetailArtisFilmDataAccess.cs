﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class DetailArtisFilmDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(DetailArtisFilmViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    detail_artis_film a = new detail_artis_film();
                    a.id_film = paramModel.id_film;
                    a.id_artis = paramModel.id_artis;
                    a.peran = paramModel.peran;
                    db.detail_artis_film.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<DetailArtisFilmViewModel> GetListByIdFilm(int idFilm)
        {
            List<DetailArtisFilmViewModel> result = new List<DetailArtisFilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<DetailArtisFilmViewModel> query;
                query = (from a in db.detail_artis_film
                         where a.id_film == idFilm
                         select new DetailArtisFilmViewModel
                         {
                             id = a.id,
                             id_artis = a.id_artis,
                             id_film = a.id_film,
                             peran = a.peran
                         });

                result = query.ToList();

                foreach (var item in result)
                {
                    ArtisViewModel artis = ArtisDataAccess.GetById((int)item.id_artis);
                    item.nama_artis = artis.nama_artis;
                    item.nama_negara = artis.nama_negara;
                }
            }
            return result;
        }

        public static List<DetailArtisFilmViewModel> GetListByIdArtis(int idArtis)
        {
            List<DetailArtisFilmViewModel> result = new List<DetailArtisFilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<DetailArtisFilmViewModel> query;
                query = (from a in db.detail_artis_film
                         where a.id_artis == idArtis
                         select new DetailArtisFilmViewModel
                         {
                             id = a.id,
                             id_artis = a.id_artis,
                             id_film = a.id_film,
                         });

                result = query.ToList();
                foreach (var item in result)
                {
                    item.nama_film = FilmDataAccess.GetById((int)item.id_film).nama_film;
                }
            }
            return result;
        }

        public static bool CheckBelumAda(int idFilm, int idArtis)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                detail_artis_film a = db.detail_artis_film.Where(
                    o => o.id_film == idFilm && o.id_artis == idArtis
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
