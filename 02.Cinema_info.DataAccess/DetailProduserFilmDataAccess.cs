﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class DetailProduserFilmDataAccess
    {
        public static string Message = string.Empty;
        public static bool Insert(DetailProduserFilmViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    detail_produser_film a = new detail_produser_film();
                    a.id_film = paramModel.id_film;
                    a.id_produser = paramModel.id_produser;                    
                    db.detail_produser_film.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static List<DetailProduserFilmViewModel> GetListByIdFilm(int idFilm)
        {
            List<DetailProduserFilmViewModel> result = new List<DetailProduserFilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<DetailProduserFilmViewModel> query;
                query = (from a in db.detail_produser_film
                         where a.id_film == idFilm
                         select new DetailProduserFilmViewModel
                         {
                             id = a.id,
                             id_produser = a.id_produser,
                             id_film = a.id_film,
                         });

                result = query.ToList();

                foreach (var item in result)
                {
                    item.nama_produser = ProduserDataAccess.GetById((int)item.id_produser).nama_produser;
                }
            }
            return result;
        }

        public static List<DetailProduserFilmViewModel> GetListByIdProd(int idProd)
        {
            List<DetailProduserFilmViewModel> result = new List<DetailProduserFilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<DetailProduserFilmViewModel> query;
                query = (from a in db.detail_produser_film
                         where a.id_produser == idProd
                         select new DetailProduserFilmViewModel
                         {
                             id = a.id,
                             id_produser = a.id_produser,
                             id_film = a.id_film,
                         });

                result = query.ToList();
                foreach (var item in result)
                {
                    item.nama_film = FilmDataAccess.GetById((int)item.id_film).nama_film;
                }
            }
            return result;
        }
    }
}
