﻿using _03.Cinema_Info.ViewModel;
using _04.Cinema_Info.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.Cinema_info.DataAccess
{
    public class FilmDataAccess
    {
        public static string Message = string.Empty;

        public static bool Insert(FilmViewModel paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new CinemaEntities())
                {
                    tbl_t_film film = new tbl_t_film();
                    film.kode_film = "KODE";
                    film.nama_film = paramModel.nama_film;
                    film.pendapatan = paramModel.pendapatan;
                    film.nominasi = paramModel.nominasi;
                    film.tahun_rilis = paramModel.tahun_rilis;
                    film.is_active = true;
                    film.created_by = paramModel.created_by;
                    film.created_date = DateTime.UtcNow;
                    film.updated_by = paramModel.created_by;
                    film.updated_date = DateTime.UtcNow;
                    db.tbl_t_film.Add(film);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

        public static bool CheckKodeFilm(string kode)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                tbl_t_film a = db.tbl_t_film.Where(
                    o =>
                    o.kode_film == kode
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool CheckJudulFilm(string judul)
        {
            bool result = true;
            using (var db = new CinemaEntities())
            {
                tbl_t_film a = db.tbl_t_film.Where(
                    o =>
                    o.nama_film.ToLower() == judul.ToLower() 
                    ).FirstOrDefault();

                if (a != null)
                {
                    result = false;
                }
            }
            return result;
        }

        public static List<FilmViewModel> GetList(string paramSearch, int paramPage, int paramPageSize)
        {
            List<FilmViewModel> result = new List<FilmViewModel>();
            using (var db = new CinemaEntities())
            {
                IQueryable<FilmViewModel> query;
                query = (from film in db.tbl_t_film
                         where film.is_active
                         && (film.kode_film.ToLower().Trim().Contains(paramSearch)
                         || film.nama_film.ToLower().Trim().Contains(paramSearch)
                         //|| film.tgl_surat.Value.Month == bulan
                         )
                         orderby film.tahun_rilis descending 
                         select new FilmViewModel
                         {
                             id_film_pk = film.id_film_pk,
                             kode_film = film.kode_film,
                             nama_film = film.nama_film,
                             pendapatan = film.pendapatan,
                             nominasi = film.nominasi,
                             tahun_rilis = film.tahun_rilis,
                         }).Skip(paramPage).Take(paramPageSize);

                result = query.ToList();
            }
            return result;
        }

        public static int GetCount(string paramSearch)
        {
            int countData = 0;
            using (var db = new CinemaEntities())
            {
                countData = (from film in db.tbl_t_film
                             where film.is_active
                             && (film.kode_film.ToLower().Trim().Contains(paramSearch)
                             || film.nama_film.ToLower().Trim().Contains(paramSearch)
                             )
                             select new FilmViewModel { }).Count();
            }
            return countData;
        }

        public static FilmViewModel GetById(int idFilm)
        {
            FilmViewModel result = new FilmViewModel();
            using (var db = new CinemaEntities())
            {
                result = (from film in db.tbl_t_film
                         where film.is_active
                         && film.id_film_pk == idFilm
                         select new FilmViewModel
                         {
                             id_film_pk = film.id_film_pk,
                             kode_film = film.kode_film,
                             nama_film = film.nama_film,
                             pendapatan = film.pendapatan,
                             nominasi = film.nominasi,
                             tahun_rilis = film.tahun_rilis,
                         }).FirstOrDefault();
            }
            return result;
        }
    }
}
