namespace _04.Cinema_Info.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_m_produser
    {
        [Key]
        public int id_produser_pk { get; set; }

        [Required]
        [StringLength(255)]
        public string kode_produser { get; set; }

        [Required]
        [StringLength(255)]
        public string nama_produser { get; set; }

        [Required]
        [StringLength(255)]
        public string international { get; set; }

        public int id_negara { get; set; }

        public bool is_active { get; set; }

        [Required]
        [StringLength(255)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        [Required]
        [StringLength(255)]
        public string updated_by { get; set; }

        public DateTime updated_date { get; set; }
    }
}
