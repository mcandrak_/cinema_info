namespace _04.Cinema_Info.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class detail_artis_film
    {
        public int id { get; set; }

        public int? id_film { get; set; }

        public int? id_artis { get; set; }

        [StringLength(50)]
        public string peran { get; set; }
    }
}
