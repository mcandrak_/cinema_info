namespace _04.Cinema_Info.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CinemaEntities : DbContext
    {
        public CinemaEntities()
            : base("name=CinemaEntities")
        {
        }

        public virtual DbSet<detail_artis_film> detail_artis_film { get; set; }
        public virtual DbSet<detail_genre_film> detail_genre_film { get; set; }
        public virtual DbSet<detail_produser_film> detail_produser_film { get; set; }
        public virtual DbSet<tbl_m_artis> tbl_m_artis { get; set; }
        public virtual DbSet<tbl_m_genre> tbl_m_genre { get; set; }
        public virtual DbSet<tbl_m_negara> tbl_m_negara { get; set; }
        public virtual DbSet<tbl_m_produser> tbl_m_produser { get; set; }
        public virtual DbSet<tbl_t_film> tbl_t_film { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<detail_artis_film>()
                .Property(e => e.peran)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_m_artis>()
                .Property(e => e.bayaran)
                .HasPrecision(18, 0);
        }
    }
}
