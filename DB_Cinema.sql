USE [DB_Cinema]
GO
/****** Object:  Table [dbo].[detail_artis_film]    Script Date: 26/05/2020 20:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detail_artis_film](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_film] [int] NULL,
	[id_artis] [int] NULL,
	[peran] [varchar](50) NULL,
 CONSTRAINT [PK_detail_artis_film_baru] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detail_genre_film]    Script Date: 26/05/2020 20:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detail_genre_film](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_film] [int] NULL,
	[id_genre] [int] NULL,
 CONSTRAINT [PK_tbl_detail_genre] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detail_produser_film]    Script Date: 26/05/2020 20:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detail_produser_film](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_film] [int] NULL,
	[id_produser] [int] NULL,
 CONSTRAINT [PK_detail_produser_film] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_m_artis]    Script Date: 26/05/2020 20:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_m_artis](
	[id_artis_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_artis] [nvarchar](255) NOT NULL,
	[nama_artis] [nvarchar](255) NOT NULL,
	[jenis_kelamin] [nvarchar](255) NOT NULL,
	[bayaran] [decimal](18, 0) NOT NULL,
	[award] [int] NOT NULL,
	[id_negara] [int] NOT NULL,
	[is_active] [bit] NOT NULL,
	[created_by] [nvarchar](255) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [nvarchar](255) NOT NULL,
	[updated_date] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_m_artis_baru_2] PRIMARY KEY CLUSTERED 
(
	[id_artis_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_m_genre]    Script Date: 26/05/2020 20:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_m_genre](
	[id_genre_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_genre] [nvarchar](255) NOT NULL,
	[nama_genre] [nvarchar](255) NOT NULL,
	[is_active] [bit] NOT NULL,
	[created_by] [nvarchar](255) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [nvarchar](255) NOT NULL,
	[updated_date] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_m_genre_new] PRIMARY KEY CLUSTERED 
(
	[id_genre_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_m_negara]    Script Date: 26/05/2020 20:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_m_negara](
	[id_negara_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_negara] [nvarchar](255) NOT NULL,
	[nama_negara] [nvarchar](255) NOT NULL,
	[is_active] [bit] NOT NULL,
	[created_by] [nvarchar](255) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [nvarchar](255) NOT NULL,
	[updated_date] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_m_negara_new] PRIMARY KEY CLUSTERED 
(
	[id_negara_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_m_produser]    Script Date: 26/05/2020 20:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_m_produser](
	[id_produser_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_produser] [nvarchar](255) NOT NULL,
	[nama_produser] [nvarchar](255) NOT NULL,
	[international] [nvarchar](255) NOT NULL,
	[id_negara] [int] NOT NULL,
	[is_active] [bit] NOT NULL,
	[created_by] [nvarchar](255) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [nvarchar](255) NOT NULL,
	[updated_date] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_m_produser_baru] PRIMARY KEY CLUSTERED 
(
	[id_produser_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_t_film]    Script Date: 26/05/2020 20:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_film](
	[id_film_pk] [int] IDENTITY(1,1) NOT NULL,
	[kode_film] [nvarchar](255) NOT NULL,
	[nama_film] [nvarchar](255) NOT NULL,
	[pendapatan] [decimal](18, 2) NOT NULL,
	[nominasi] [int] NOT NULL,
	[tahun_rilis] [datetime] NOT NULL,
	[is_active] [bit] NOT NULL,
	[created_by] [nvarchar](255) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_by] [nvarchar](255) NOT NULL,
	[updated_date] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_t_film_baru_1] PRIMARY KEY CLUSTERED 
(
	[id_film_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[detail_artis_film] ON 

INSERT [dbo].[detail_artis_film] ([id], [id_film], [id_artis], [peran]) VALUES (1, 5, 1, N'Xin')
INSERT [dbo].[detail_artis_film] ([id], [id_film], [id_artis], [peran]) VALUES (2, 3, 1, N'Xin')
INSERT [dbo].[detail_artis_film] ([id], [id_film], [id_artis], [peran]) VALUES (3, 5, 3, N'Piao dan Ying Zheng')
INSERT [dbo].[detail_artis_film] ([id], [id_film], [id_artis], [peran]) VALUES (4, 5, 4, N'He Liao Diao')
INSERT [dbo].[detail_artis_film] ([id], [id_film], [id_artis], [peran]) VALUES (5, 5, 5, N'Yang Duan He')
INSERT [dbo].[detail_artis_film] ([id], [id_film], [id_artis], [peran]) VALUES (6, 2, 2, N'Min-hyuk')
SET IDENTITY_INSERT [dbo].[detail_artis_film] OFF
SET IDENTITY_INSERT [dbo].[tbl_m_artis] ON 

INSERT [dbo].[tbl_m_artis] ([id_artis_pk], [kode_artis], [nama_artis], [jenis_kelamin], [bayaran], [award], [id_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'KODE', N'Kento Yamazaki', N'L', CAST(56000000 AS Decimal(18, 0)), 7, 7, 1, N'admin', CAST(N'2020-05-25T23:07:22.020' AS DateTime), N'admin', CAST(N'2020-05-25T23:07:22.020' AS DateTime))
INSERT [dbo].[tbl_m_artis] ([id_artis_pk], [kode_artis], [nama_artis], [jenis_kelamin], [bayaran], [award], [id_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'KODE', N'Park Seo Jonn', N'L', CAST(45000000 AS Decimal(18, 0)), 10, 8, 1, N'admin', CAST(N'2020-05-26T11:29:46.313' AS DateTime), N'admin', CAST(N'2020-05-26T11:29:46.313' AS DateTime))
INSERT [dbo].[tbl_m_artis] ([id_artis_pk], [kode_artis], [nama_artis], [jenis_kelamin], [bayaran], [award], [id_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'KODE', N'Ryo Yoshizawa', N'L', CAST(56000000 AS Decimal(18, 0)), 6, 7, 1, N'admin', CAST(N'2020-05-26T12:14:27.410' AS DateTime), N'admin', CAST(N'2020-05-26T12:14:27.410' AS DateTime))
INSERT [dbo].[tbl_m_artis] ([id_artis_pk], [kode_artis], [nama_artis], [jenis_kelamin], [bayaran], [award], [id_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'KODE', N'Kanna Hashimoto', N'P', CAST(67000000 AS Decimal(18, 0)), 7, 7, 1, N'admin', CAST(N'2020-05-26T12:16:18.230' AS DateTime), N'admin', CAST(N'2020-05-26T12:16:18.230' AS DateTime))
INSERT [dbo].[tbl_m_artis] ([id_artis_pk], [kode_artis], [nama_artis], [jenis_kelamin], [bayaran], [award], [id_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, N'KODE', N'Masami Nagasawa ', N'P', CAST(56000000 AS Decimal(18, 0)), 6, 7, 1, N'admin', CAST(N'2020-05-26T12:52:32.223' AS DateTime), N'admin', CAST(N'2020-05-26T12:52:32.223' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_m_artis] OFF
SET IDENTITY_INSERT [dbo].[tbl_m_genre] ON 

INSERT [dbo].[tbl_m_genre] ([id_genre_pk], [kode_genre], [nama_genre], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'G001', N'ACTION', 1, N'system', CAST(N'2019-10-30T13:48:49.873' AS DateTime), N'system', CAST(N'2019-10-30T13:48:49.873' AS DateTime))
INSERT [dbo].[tbl_m_genre] ([id_genre_pk], [kode_genre], [nama_genre], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'G002', N'HORROR', 1, N'system', CAST(N'2019-10-30T13:48:49.873' AS DateTime), N'system', CAST(N'2019-10-30T13:48:49.873' AS DateTime))
INSERT [dbo].[tbl_m_genre] ([id_genre_pk], [kode_genre], [nama_genre], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'G003', N'COMEDY', 1, N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime), N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime))
INSERT [dbo].[tbl_m_genre] ([id_genre_pk], [kode_genre], [nama_genre], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'G004', N'DRAMA', 1, N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime), N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime))
INSERT [dbo].[tbl_m_genre] ([id_genre_pk], [kode_genre], [nama_genre], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, N'G005', N'THRILLER', 1, N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime), N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime))
INSERT [dbo].[tbl_m_genre] ([id_genre_pk], [kode_genre], [nama_genre], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (6, N'G006', N'FICTION', 1, N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime), N'system', CAST(N'2019-10-30T13:48:49.877' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_m_genre] OFF
SET IDENTITY_INSERT [dbo].[tbl_m_negara] ON 

INSERT [dbo].[tbl_m_negara] ([id_negara_pk], [kode_negara], [nama_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (1, N'AS', N'AMERIKA SERIKAT', 1, N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime), N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime))
INSERT [dbo].[tbl_m_negara] ([id_negara_pk], [kode_negara], [nama_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'HK', N'HONG KONG', 1, N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime), N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime))
INSERT [dbo].[tbl_m_negara] ([id_negara_pk], [kode_negara], [nama_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (3, N'ID', N'INDONESIA', 1, N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime), N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime))
INSERT [dbo].[tbl_m_negara] ([id_negara_pk], [kode_negara], [nama_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (4, N'IN', N'INDIA', 1, N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime), N'system', CAST(N'2019-10-31T15:20:38.603' AS DateTime))
INSERT [dbo].[tbl_m_negara] ([id_negara_pk], [kode_negara], [nama_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (7, N'JP', N'JEPANG', 1, N'system', CAST(N'2020-05-26T00:00:00.000' AS DateTime), N'system', CAST(N'2020-05-26T00:00:00.000' AS DateTime))
INSERT [dbo].[tbl_m_negara] ([id_negara_pk], [kode_negara], [nama_negara], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (8, N'KODE', N'KOREA SELATAN', 1, N'admin', CAST(N'2020-05-26T11:28:50.070' AS DateTime), N'admin', CAST(N'2020-05-26T11:28:50.070' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_m_negara] OFF
SET IDENTITY_INSERT [dbo].[tbl_t_film] ON 

INSERT [dbo].[tbl_t_film] ([id_film_pk], [kode_film], [nama_film], [pendapatan], [nominasi], [tahun_rilis], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (2, N'FFM1', N'Parasite', CAST(570000000.00 AS Decimal(18, 2)), 5, CAST(N'2020-01-01T00:00:00.000' AS DateTime), 1, N'1', CAST(N'2020-04-20T02:32:35.437' AS DateTime), N'1', CAST(N'2020-04-20T02:32:35.437' AS DateTime))
INSERT [dbo].[tbl_t_film] ([id_film_pk], [kode_film], [nama_film], [pendapatan], [nominasi], [tahun_rilis], [is_active], [created_by], [created_date], [updated_by], [updated_date]) VALUES (5, N'KODE', N'Kingdom', CAST(57587587.00 AS Decimal(18, 2)), 67, CAST(N'2020-05-26T00:00:00.000' AS DateTime), 1, N'admin', CAST(N'2020-05-25T21:28:35.257' AS DateTime), N'admin', CAST(N'2020-05-25T21:28:35.257' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_t_film] OFF
