﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Cinema_Info.ViewModel
{
    public class GenreViewModel
    {
        [Key]
        public int id_genre_pk { get; set; }

        [Required]
        [StringLength(255)]
        public string kode_genre { get; set; }

        [Required]
        [StringLength(255)]
        public string nama_genre { get; set; }

        public bool is_active { get; set; }

        [Required]
        [StringLength(255)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        [Required]
        [StringLength(255)]
        public string updated_by { get; set; }

        public DateTime updated_date { get; set; }
    }
}
