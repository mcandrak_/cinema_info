﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Cinema_Info.ViewModel
{
    public class DetailProduserFilmViewModel
    {
        public int id { get; set; }

        public int? id_film { get; set; }

        public string nama_film { get; set; }

        public int? id_produser { get; set; }

        public string nama_produser { get; set; }
    }
}
