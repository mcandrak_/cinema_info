﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Cinema_Info.ViewModel
{
    public class FilmViewModel
    {
        //[Key]
        public int id_film_pk { get; set; }

        //[Required]
        //[StringLength(255)]
        public string kode_film { get; set; }

        //[Required]
        //[StringLength(255)]
        public string nama_film { get; set; }

        public decimal pendapatan { get; set; }

        public string pendapatan_str { get; set; }

        public int nominasi { get; set; }

        public DateTime tahun_rilis { get; set; }

        public bool is_active { get; set; }

        //[Required]
        //[StringLength(255)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        //[Required]
        //[StringLength(255)]
        public string updated_by { get; set; }

        public DateTime updated_date { get; set; }
    }

    public class PagingModel_Film
    {
        public PagingModel_Film(int totalItems, int? page, int pageSize = 10)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentPage = page != null ? (int)page : 1;
            var startPage = currentPage - 5;
            var endPage = currentPage + 4;
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }
            TotalItems = totalItems;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
        }
        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
    }
}
