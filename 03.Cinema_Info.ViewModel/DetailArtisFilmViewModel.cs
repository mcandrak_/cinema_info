﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Cinema_Info.ViewModel
{
    public class DetailArtisFilmViewModel
    {
        public int id { get; set; }

        public int? id_film { get; set; }

        public string nama_film { get; set; }

        public int? id_artis { get; set; }

        public string nama_artis { get; set; }
        public string nama_negara { get; set; }

        [StringLength(50)]
        public string peran { get; set; }
    }
}
