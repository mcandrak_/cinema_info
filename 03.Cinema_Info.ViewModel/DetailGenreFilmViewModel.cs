﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.Cinema_Info.ViewModel
{
    public class DetailGenreFilmViewModel
    {
        public int id { get; set; }

        public int? id_film { get; set; }

        public int? id_genre { get; set; }

        public string nama_genre { get; set; }
        public string nama_film { get; set; }
    }
}
