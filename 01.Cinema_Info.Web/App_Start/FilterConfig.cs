﻿using System.Web;
using System.Web.Mvc;

namespace _01.Cinema_Info.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
