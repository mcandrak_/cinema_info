﻿using System;
using _02.Cinema_info.DataAccess;
using _03.Cinema_Info.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace _01.Cinema_Info.Web.Controllers
{
    public class ArtisController : Controller
    {
        // GET: Artis
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddToFilm(int idFilm)
        {
            ViewBag.idFilm = idFilm;
            ViewBag.judulFilm = FilmDataAccess.GetById(idFilm).nama_film;
            List<ArtisViewModel> artis = ArtisDataAccess.GetAll();
            foreach (var item in artis)
            {
                item.belum_masuk_list = DetailArtisFilmDataAccess.CheckBelumAda(idFilm, item.id_artis_pk);
            }
            return PartialView("AddToFilm", artis);
        }

        public ActionResult AddPeran(int idFilm, int idArtis)
        {
            ViewBag.idFilm = idFilm;
            ViewBag.idArtis = idArtis;
            ViewBag.judulFilm = FilmDataAccess.GetById(idFilm).nama_film;
            ViewBag.namaArtis = ArtisDataAccess.GetById(idArtis).nama_artis;


            return PartialView("AddPeran");
        }

        [HttpPost]
        public ActionResult AddToFilm(DetailArtisFilmViewModel paramModel)
        {
            try
            {
                if (DetailArtisFilmDataAccess.Insert(paramModel))
                {
                    return Json(new { success = true, message = "berhasil!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = FilmDataAccess.Message }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Add()
        {
            ViewData["Negara"] = NegaraDataAccess.GetAll();
            return PartialView("Add");
        }

        [HttpPost]
        public ActionResult Add(ArtisViewModel paramModel)
        {
            paramModel.created_by = "admin";
            try
            {
                if (ArtisDataAccess.Insert(paramModel))
                {
                    return Json(new { success = true, message = "berhasil!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = FilmDataAccess.Message }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult List(int? paramPage, int paramPageSize, string paramSearch)
        {
            int countDataDb = ArtisDataAccess.GetCount(paramSearch);
            PagingModel_Artis pg = new PagingModel_Artis(countDataDb, paramPage, paramPageSize);
            ViewData["pg"] = pg;
            List<ArtisViewModel> listData = ArtisDataAccess.GetList(paramSearch, ((pg.CurrentPage - 1) * pg.PageSize), pg.PageSize);
            foreach (var item in listData)
            {
                int angka = Convert.ToInt32(item.bayaran);
                item.bayaran_str = String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
            }
            return PartialView("List", listData);
        }
        
        public ActionResult CheckNamaArtis(string nama, string jk, int idN)
        {
            if (ArtisDataAccess.CheckArtis(nama, jk, idN))
            {
                return Json(new { success = true, message = "none" }, JsonRequestBehavior.AllowGet);//belum ada
            }
            else
            {
                return Json(new { success = false, message = "exist" }, JsonRequestBehavior.AllowGet);//sudah ada
            }
        }
    }
}