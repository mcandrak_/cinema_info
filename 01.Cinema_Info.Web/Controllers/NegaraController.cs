﻿using _02.Cinema_info.DataAccess;
using _03.Cinema_Info.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _01.Cinema_Info.Web.Controllers
{
    public class NegaraController : Controller
    {
        // GET: Negara
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            return PartialView("Add");
        }

        [HttpPost]
        public ActionResult Add(NegaraViewModel paramModel)
        {
            paramModel.created_by = "admin";
            try
            {
                if (NegaraDataAccess.Insert(paramModel))
                {
                    return Json(new { success = true, message = "berhasil!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = FilmDataAccess.Message }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult List(int? paramPage, int paramPageSize, string paramSearch)
        {
            int countDataDb = NegaraDataAccess.GetCount(paramSearch);
            PagingModel_Negara pg = new PagingModel_Negara(countDataDb, paramPage, paramPageSize);
            ViewData["pg"] = pg;
            List<NegaraViewModel> listData = NegaraDataAccess.GetList(paramSearch, ((pg.CurrentPage - 1) * pg.PageSize), pg.PageSize);
            return PartialView("List", listData);
        }

        public ActionResult CheckNegara(string nama)
        {
            if (NegaraDataAccess.CheckNegara(nama))
            {
                return Json(new { success = true, message = "none" }, JsonRequestBehavior.AllowGet);//belum ada
            }
            else
            {
                return Json(new { success = false, message = "exist" }, JsonRequestBehavior.AllowGet);//sudah ada
            }
        }
    }
}