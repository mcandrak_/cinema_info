﻿using _02.Cinema_info.DataAccess;
using _03.Cinema_Info.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _01.Cinema_Info.Web.Controllers
{
    public class FilmController : Controller
    {
        // GET: Film
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create() 
        {
            return PartialView("Create");
        }

        [HttpPost]
        public ActionResult Create(FilmViewModel paramModel)
        {
            paramModel.created_by = "admin";
            try
            {
                if (FilmDataAccess.Insert(paramModel))
                {
                    return Json(new { success = true, message = "berhasil!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = FilmDataAccess.Message }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult List(int? paramPage, int paramPageSize, string paramSearch)
        {
            int countDataDb = FilmDataAccess.GetCount(paramSearch);
            PagingModel_Film pg = new PagingModel_Film(countDataDb, paramPage, paramPageSize);
            ViewData["pg"] = pg;
            List<FilmViewModel> listData = FilmDataAccess.GetList(paramSearch, ((pg.CurrentPage - 1) * pg.PageSize), pg.PageSize);
            foreach (var item in listData)
            {
                int angka = Convert.ToInt32(item.pendapatan);
                item.pendapatan_str = String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
            }
            return PartialView("List", listData);
        }

        public ActionResult CheckKodeFilm(string kode)
        {
            if (FilmDataAccess.CheckKodeFilm(kode.ToUpper()))
            {
                return Json(new { success = true, message = "none" }, JsonRequestBehavior.AllowGet);//belum ada
            }
            else
            {
                return Json(new { success = false, message = "exist" }, JsonRequestBehavior.AllowGet);//sudah ada
            }
        }

        public ActionResult CheckJudulFilm(string judul)
        {
            if (FilmDataAccess.CheckJudulFilm(judul.ToUpper()))
            {
                return Json(new { success = true, message = "none" }, JsonRequestBehavior.AllowGet);//belum ada
            }
            else
            {
                return Json(new { success = false, message = "exist" }, JsonRequestBehavior.AllowGet);//sudah ada
            }
        }

        public ActionResult Detail(int idFilm)
        {
            ViewData["artis"] = DetailArtisFilmDataAccess.GetListByIdFilm(idFilm);
            FilmViewModel film = FilmDataAccess.GetById(idFilm);
            int angka = Convert.ToInt32(film.pendapatan);
            film.pendapatan_str = String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
            return PartialView("Detail", film);
        }
    }
}